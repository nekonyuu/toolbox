#!/usr/bin/env python2
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('rmq1'))
channel = connection.channel()
channel.queue_declare(queue='hello')
val = 1
while 1:
    channel.basic_publish(exchange='', routing_key='hello', body="Hello World %s!" % val)
    print " [x] Sent 'Hello World %s!'" % val
    val = val + 1

connection.close()
