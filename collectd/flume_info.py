#!/usr/bin/env python

# Author: Jonathan Raffre <nk@nyuu.eu>
# Purpose: fetch statistics on Flume and send them to collectd
# Dependencies : collectd (should be run by collectd's python plugin)
# Compatibility: Python 2.7 / 3.3

from __future__ import print_function
import json
from optparse import OptionParser
import re

import collectd

try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

FLUME_URL = "http://localhost:34545/metrics"
VERBOSE_LOGGING = False

METRICS = {
    # Channel Metrics
    'ChannelSize': 'gauge',
    'ChannelCapacity': 'gauge',
    'ChannelFillPercentage': 'gauge',
    'EventPutAttemptCount': 'counter',
    'EventTakeSuccessCount': 'counter',
    'EventTakeAttemptCount': 'counter',
    'EventPutSuccessCount': 'counter',
    'ConnectionFailedCount': 'counter',

    # Sink Metrics
    'ConnectionFailedCount': 'counter',
    'ConnectionClosedCount': 'counter',
    'ConnectionCreatedCount': 'counter',
    'EventDrainAttemptCount': 'counter',
    'BatchCompleteCount': 'counter',
    'EventDrainSuccessCount': 'counter',
    'BatchUnderflowCount': 'counter',
    'BatchEmptyCount': 'counter',

    # Source Metrics
    'EventReceivedCount': 'counter',
    'OpenConnectionCount': 'counter',
    'AppendBatchReceivedCount': 'counter',
    'AppendBatchAcceptedCount': 'counter',
    'EventAcceptedCount': 'counter',
    'AppendAcceptedCount': 'counter',
    'AppendReceivedCount': 'counter',
}

def log_verbose(msg):
    if not VERBOSE_LOGGING:
        return
    collectd.info('flume plugin [verbose]: %s' % msg)

def fetch_info():
    output = urlopen(FLUME_URL)
    metrics = json.loads(output.read().decode())
    return metrics

def extract_metrics(metrics):
    channelList = [ key for key in metrics.keys() if re.match("^CHANNEL", key) ]
    fillstate = {}
    for key in channelList:
        fillstate[key] = float(metrics[key]["ChannelFillPercentage"])
    return fillstate

def configure_callback(conf):
    """Receive configuration block"""
    global FLUME_URL, VERBOSE_LOGGING
    for node in conf.children:
        if node.key == 'MetricsURL':
            REDIS_HOST = node.values[0]
        elif node.key == 'Verbose':
            VERBOSE_LOGGING = bool(node.values[0])
        else:
            collectd.warning('flume_info plugin: Unknown config key: %s.'
                             % node.key)
    log_verbose('Configured with MetricsURL=%s' % FLUME_URL)

def dispatch_value(info, type, key):
    """Read a key from info response data and dispatch a value"""
    if key not in info[type]:
        collectd.warning('flume_info plugin: Info key not found in %s : %s' % (type, key))
        return

    value = int(float(info[type][key]))
    log_verbose('Sending value: %s=%s' % (key, value))

    val = collectd.Values(plugin='flume_info')
    val.plugin_instance = type
    val.type = METRICS[key]
    val.type_instance = key
    val.values = [value]
    val.dispatch()

def read_callback():
    log_verbose('Read callback called')
    info = fetch_info()

    if not info:
        collectd.error('flume plugin: No info received')
        return

    for type, metrics in info.items():
        metricsList = metrics.keys()
        metricsList.remove('Type')
        metricsList.remove('StopTime')
        metricsList.remove('StartTime')
        for key in metricsList:
            dispatch_value(info, type, key)

# register callbacks
collectd.register_config(configure_callback)
collectd.register_read(read_callback)
