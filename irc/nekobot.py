#!/usr/bin/env python2

import irc.bot
import irc.client
import re
import ssl
import chardet
import codecs

nicklist = ['yolotroll']
regexps = [('said', 'response')]
channels = [('#channel', 'channel_key')]

# Parameters
nickname = 'NekoBot'
fullname = 'IMMA FIRING MAH LAZER !!'
server = ('irc.freenode.net', 6697)
use_ssl = True

class NekoBot(irc.bot.SingleServerIRCBot):
    def __init__(self):
        irc.client.ServerConnection.buffer_class = irc.client.LineBuffer
        if use_ssl:
            factory = irc.connection.Factory(wrapper=ssl.wrap_socket)
            irc.bot.SingleServerIRCBot.__init__(self, [server], nickname, fullname, connect_factory = factory)
        else:
            irc.bot.SingleServerIRCBot.__init__(self, [server], nickname, fullname)

    def on_welcome(self, serv, ev):
        for chan, key in channels:
            serv.join(chan, key)

    def on_kick(self, serv, ev):
        channel = ev.target
        for chan, key in channels:
            if channel == chan:
                serv.join(chan, key)

    def on_pubmsg(self, serv, ev):
        msg_raw = ev.arguments[0].lower()
        try:
            msg = msg_raw.decode('utf-8')
        except UnicodeError:
            msg = msg_raw.decode('iso-8859-15')
        mask = ev.source
        nick = ev.source.nick
        channel = ev.target
        print "%s said %s" % (nick, msg)
        for nickregexp in nicklist:
            if re.match(nickregexp, mask.lower()):
                for regexp, response in regexps:
                    if re.match(regexp, msg):
                        # print "Kicking %s from %s because said '%s' !" % (nick, channel, msg)
                        serv.privmsg(channel, response)

if __name__ == '__main__':
    NekoBot().start()
