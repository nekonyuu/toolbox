#!/usr/bin/env python2

import ldap
import os

# Parameters
ldap_server = 'ldap://ldap-serber:389'
ldap_admin_userdn = 'cn=admin,dc=demo,dc=net'
ldap_admin_password = 'password'

postfixAliasFile = 'ldap-group.cf'
groupAliasList = open(postfixAliasFile, 'w')

try:
    l = ldap.initialize(ldap_server)
    # TLS Mode
    # l.set_option(ldap.OPT_X_TLS_CACERTFILE, '/etc/pki/ca-cert.crt')
    # l.start_tls_s()
    l.simple_bind_s(ldap_admin_userdn, ldap_admin_password)
except ldap.LDAPError, e:
    print "Could not bind to LDAP server with given credentials, error was : %s" % e.message
    exit(1)

baseDN = 'dc=demo,dc=net'
groupSearchFilter = '(&(objectClass=zarafa-group))'
searchScope = ldap.SCOPE_SUBTREE
retrieveAttributes = None
oldPasswords = []

try:
    search_result = l.search_s(baseDN, searchScope, groupSearchFilter, retrieveAttributes)

except ldap.LDAPError, e:
    print "Could not search in LDAP server, error was : %s" % e.message


for dn, groupinfo in search_result:
    if 'memberUid' in groupinfo and 'mail' in groupinfo:
        membersUid = groupinfo['memberUid']
        groupMail = groupinfo['mail'][0]
        membersMails = []
        for uid in membersUid:
            uidSearchFilter = '(uid=%s)' % uid
            uidSearchResult = l.search_s(baseDN, searchScope, uidSearchFilter, retrieveAttributes)
            for memberDN, attrs in uidSearchResult:
                membersMails.append(attrs['mail'][0])
        # Writing alias line for group
        groupAliasList.write("%s    %s\n" % (groupMail, ','.join(membersMails)))

groupAliasList.close()

# Updating postmap
os.system('postmap %s' % postfixAliasFile)
