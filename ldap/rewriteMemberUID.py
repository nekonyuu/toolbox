#!/usr/bin/env python2

import ldap

# Parameters
ldap_server = 'ldap://ldap-dev:389'
ldap_admin_userdn = 'cn=admin,dc=demo,dc=net'
ldap_admin_password = 'password'

try:
    l = ldap.initialize(ldap_server)
    # TLS Mode
    # l.set_option(ldap.OPT_X_TLS_CACERTFILE, '/etc/pki/ca-cert.crt')
    # l.start_tls_s()
    l.simple_bind_s(ldap_admin_userdn, ldap_admin_password)
except ldap.LDAPError, e:
    print "Could not bind to LDAP server with given credentials, error was : %s" % e.message
    exit(1)

baseDN = 'dc=demo,dc=net'
groupSearchFilter = '(&(objectClass=zarafa-group))'
searchScope = ldap.SCOPE_SUBTREE
retrieveAttributes = None
oldPasswords = []

try:
    search_result = l.search_s(baseDN, searchScope, groupSearchFilter, retrieveAttributes)

except ldap.LDAPError, e:
    print "Could not search in LDAP server, error was : %s" % e.message


for dn, userinfo in search_result:
    if 'memberUid' in userinfo:
        membersUid = userinfo['memberUid']
        membersDN = []
        for uid in membersUid:
            uidSearchFilter = '(uid=%s)' % uid
            uidSearchResult = l.search_s(baseDN, searchScope, uidSearchFilter, retrieveAttributes)
            for memberDN, attrs in uidSearchResult:
                membersDN.append(memberDN)
        # Rewriting memberList on LDAP
        print "Rewriting members list for DN = %s with members = %s" % (dn, ','.join(membersUid))
        l.modify_s(dn, [(ldap.MOD_REPLACE, 'memberUid', membersDN)])

