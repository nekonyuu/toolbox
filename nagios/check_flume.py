#!/usr/bin/env python

# Author: Jonathan Raffre <jonathan.raffre.xebia@axa.com> <jraffre@xebia.fr>
# Purpose: check channel filling status on Apache Flume via the metrics HTTP-JSON API
# Dependencies : none
# Compatibility: Python 2.7 / 3.3

from __future__ import print_function
import json
from optparse import OptionParser
import re

try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

def parse_args():
    usage = "usage: %prog -u url -w warning_threshold -c critical_threshold"
    parser = OptionParser(usage=usage, add_help_option=True)
    parser.set_defaults(verbose=True)
    parser.add_option("-u", "--url", dest="url",
                      help="URL to pull JSON from")
    parser.add_option("-w", "--warning", dest="warning",
                      default=75,
                      help="Warning threshold on the ChannelFillPercentage")
    parser.add_option("-c", "--critical", dest="critical",
                      default=85,
                      help="Critical threshold on the ChannelFillPercentage")

    options, args = parser.parse_args()
    if not options.url:
        parser.error("Metrics URL required")

    if int(options.warning) >= int(options.critical):
        parser.error("The warning threshold cannot be higher or equal to the critical one")

    return options, args

def extract_metrics(url):
    output = urlopen(url)
    metrics = json.loads(output.read().decode())
    # Retrieve ChannelFillPercentage metrics for each channel
    channelList = [ key for key in metrics.keys() if re.match("^CHANNEL", key) ]
    fillstate = {}
    for key in channelList:
        fillstate[key] = float(metrics[key]["ChannelFillPercentage"])
    return fillstate

def triggers(fillstate, wthres, cthres):
    rcode = 0
    # alert if we have at least one threshold triggered
    for key, chanfill in fillstate.items():
        if chanfill >= wthres and chanfill < cthres:
            status = "WARNING"
            rcode = 1 if rcode == 0 else rcode
        elif chanfill >= cthres:
            status = "CRITICAL"
            rcode = 2 if rcode == 0 else rcode
        else:
            status = "OK"
    
        message = "%s: %s is filled at %s%%" % (status, key, chanfill)
        print(message)

if __name__ == "__main__":
    options, args = parse_args()
    fillstate = extract_metrics(options.url)
    triggers(fillstate, options.warning, options.critical)
