# DNSController.py
# Author : Jonathan Raffre <nk@nyuu.eu>
# Purpose : manage DNS records remotely with DDNS protocol
# Version : 
#    - 0.1 : initial, no (or little) error management
#
# Future :
#    - 0.2 : add error management, at least
#    - 0.3 : implement PTR addition when adding A/AAAA records, toggable
#
# Code example :
#   from DNSController import DNSController
#
# Initialize
#   dnsup = DNSController('yourDNSip', 'yourDNSkeyName', 'yourDNSkey', 3600)
#
# Adding test1/test2 A records
#   dnsup.addRecord('example.com', 'test1', 'A', '192.168.122.10')
#   dnsup.addRecord('example.com', 'test2', 'A', '192.168.122.11')
#
# Updating test2 record with new IP
#   dnsup.updateRecord('example.com', 'test2', 'A', '192.168.122.100')
#
# Delete test2 record
#   dnsup.deleteRecord('example.com', 'test2', 'A')

import re
import dns
import dns.query
import dns.rdatatype
import dns.tsig
import dns.tsigkeyring
import dns.ttl
import dns.update
from dns.exception import DNSException, SyntaxError

class DNSController():
    def __init__(self, dnsServer, keyName, key, defaultTTL):
        """
        Initialize the controller with all necessary info to
        correctly communicate with the DNS server. 

        Those informations are :
          * the target DNS server
          * the key (and its name) allowed to modify the DNS server
        
        A default TTL is supplied in order to not specify the TTL 
        for each record.

        :param dnsServer: the target DNS server
        :param keyName: the name of the DNSSEC key allowed for update
        :param key: the key itself
        :param defaultTTL: the default TTL of each added record
        """
        # TODO: validate dnsServer (resolvable, at least)
        self.dnsServer = dnsServer
        self.keyring = dns.tsigkeyring.from_text({ keyName : key })
        self.defaultTTL = defaultTTL

    def addRecord(self, origin, name, recordType, value, TTL = 0):
        """
        Adding a record to the DNS server

        :param origin: DNS zone targeted
        :param name: name to add (empty for MX type, for example)
        :param recordType: the type of record as string (A, AAAA, MX, PTR)
        :param value: the value of the record
        :param TTL: the TTL of the record
        """
        updateRequest = dns.update.Update(origin, keyring=self.keyring)
        if TTL == 0: TTL = self.defaultTTL

        try:
            recordTypeE = getattr(dns.rdatatype, recordType)
        except AttributeError:
            # TODO: manage the exception, specified type does not exists
            pass

        updateRequest.add(name, TTL, recordTypeE, value)

        # Sending the update
        try:
            response = dns.query.tcp(q=updateRequest, where=self.dnsServer, timeout=10)
        except dns.tsig.PeerBadKey:
            # Wrong key
            pass
        except dns.tsig.PeerBadSignature:
            # Key malformed
            pass

    def updateRecord(self, origin, name, recordType, value, TTL = 0):
        """
        Updating a record on the DNS server

        :param origin: DNS zone targeted
        :param name: name to update (empty for MX type, for example)
        :param recordType: the type of record as string (A, AAAA, MX, PTR)
        :param value: the new value of the record
        :param TTL: the TTL of the record
        """
        updateRequest = dns.update.Update(origin, keyring=self.keyring)
        if TTL == 0: TTL = self.defaultTTL

        try:
            recordTypeE = getattr(dns.rdatatype, recordType)
        except AttributeError:
            # TODO: manage the exception, specified type does not exists
            pass

        updateRequest.replace(name, TTL, recordTypeE, value)

        # Sending the update
        try:
            response = dns.query.tcp(q=updateRequest, where=self.dnsServer, timeout=10)
        except dns.tsig.PeerBadKey:
            # Wrong key
            pass
        except dns.tsig.PeerBadSignature:
            # Key malformed
            pass
    
    def deleteRecord(self, origin, name, recordType):
        """
        Deleting a record from the DNS server

        :param origin: DNS zone targeted
        :param name: name to delete (at least the zone name for MX type)
        :param recordType: the type of record as string (A, AAAA, MX, PTR)
        """
        updateRequest = dns.update.Update(origin, keyring=self.keyring)
        
        try:
            recordTypeE = getattr(dns.rdatatype, recordType)
        except AttributeError:
            # TODO: manage the exception, specified type does not exists
            pass

        updateRequest.delete(name, recordTypeE)

        # Sending the update
        try:
            response = dns.query.tcp(q=updateRequest, where=self.dnsServer, timeout=10)
        except dns.tsig.PeerBadKey:
            # Wrong key
            pass
        except dns.tsig.PeerBadSignature:
            # Key malformed
            pass

    # Code Toolbox
    def _isValidTTL(self, TTL):
        try:
            TTL = dns.ttl.from_text(TTL)
        except:
            exit()
        return TTL

    def _isValidPTR(self, ptr):
        # TODO: implement IPv6 reverse validation
        return re.match(r'\b(?:\d{1,3}\.){3}\d{1,3}.in-addr.arpa\b', ptr)

    def _isValidV4Addr(self, addr):
        try:
            dns.ipv4.inet_aton(addr)
        except socket.error:
            return False
        return True

    def _isValidV6Addr(self, addr):
        try:
            dns.ipv6.inet_aton(addr) 
        except SyntaxError:
            return False
        return True
    
    def _isValidName(self, name):
        return re.match(r'^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9]\.?)$', name)
